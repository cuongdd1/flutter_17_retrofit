import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_17_retrofit/services/post_api_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Post> _users = <Post>[];
  List<Post> _suggestUsers = <Post>[];

  int get totalList => _suggestUsers.length;

  @override
  void initState() {
    super.initState();
  }

  void _filteringUser(String text) {
    if (text == '') {
      _suggestUsers = _users;
    } else {
      _suggestUsers = _users.where((element) {
        final title = element.title?.toLowerCase() ?? '';
        final body = element.body?.toLowerCase() ?? '';
        final key = text.toLowerCase();
        return title.contains(key) || body.contains(key);
      }).toList();
    }
    setState(() {
      
    });
  }

  Widget _buildUserListView(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: _suggestUsers.length,
        itemBuilder: (context, index) {
          return _buildCellForRowAt(index);
        },
      ),
    );
  }

  Widget _buildCellForRowAt(int index) {
    return ListTile(
      title: Text(
        '${_suggestUsers[index].title}',
        style: const TextStyle(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text('${_suggestUsers[index].body}'),
    );
  }

  Widget _buildBodyHasData(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
                icon: Icon(Icons.search), hintText: 'search something'),
                onChanged: (value) => _filteringUser(value),
          ),
          const SizedBox(height: 16),
          _buildUserListView(context),
          Text('$totalList'),
        ],
      ),
    );
  }

  FutureBuilder<List<Post>> _buildBody(BuildContext context) {
    final client =
        RestClient(Dio(BaseOptions(contentType: "application/json")));
    return FutureBuilder<List<Post>>(
      future: client.getTasks(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          _users = snapshot.data ?? [];
          _suggestUsers = _users;

          return _buildBodyHasData(context);
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _users.isEmpty ? _buildBody(context) : _buildBodyHasData(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
